#!/bin/bash	ssh username@124.40,252,256 -p2022

#set an infinite loop
while :
do
clear

#display menu
echo "Server Name - $(hostname)"
echo "--------------------------------------------"
echo "             M A I N  M E N U"
echo "--------------------------------------------"
echo "A. Aritmatika"
echo "B. Tentang Pembuat"
echo "C. Keluar Program"

#get input from the user
read -p "Enter your choice [A-C] " choice

#make decision using case..in..esac
case $choice in
	A)
		#set an infinite loop
		clear

		#display menu
		echo "-------------------------------------------"
		echo "            A R I T M A T I K A"
		echo "-------------------------------------------"
		echo "1. Penjumlahan"
		echo "2. Pengurangan"
		echo "3. Perkalian"
		echo "4. Pembagian"
		echo "5. Kembali ke Menu Utama"

		#get input from the user
		read -p "[Enter your choice [1-5] " pilihanA

		#make decision using case..in..esac
		case $pilihanA in
		1)
			echo "----- PENJUMLAHAN -----"
			echo "Masukkan angka pertama"
			read var1a
			echo "Masukkan angka kedua"
			read var2a
			let a=var1a+var2a
			echo "Hasil dari $var1a + $var2a = $a"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		2)
			echo "----- PENGURANGAN -----"
			echo "Masukkan angka pertama"
			read var1b
			echo "Masukkan angka kedua"
			read var2b
			let b=var1b-var2b
			echo "Hasil dari $var1b - $var2b = $b"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		3)
			echo "----- PERKALIAN -----"
			echo "Masukkan angka pertama"
			read var1c
			echo "Masukkan angka kedua"
			read var2c
			let c=var1c*var2c
			echo "Hasil dari $var1c x $var2c = $c"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		4)
			echo "----- PEMBAGIAN -----"
			echo "Masukkan angka pertama"
			read var1d
			echo "Masukkan angka kedua"
			read var2d
			let d=var1d/var2d
			let e=var1d%var2d
			echo "Hasil dari $var1d : $var2d = $d sisa $e"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		5)
			bash whiledo2
			;;
			*)
		echo "Error: Invalid option..."
		read -p "Press [Enter] key to continue..."
		readEnterKey
		;;
	esac
	;;

	B)
		#set an infinite loop
		clear

		#display menu
		echo "-------------------------------------------"
		echo "       T E N T A N G  P E M B U A T"
		echo "-------------------------------------------"
		echo "1. Nama, NIM, Home Dir"
		echo "2. Username@ Hostname"
		echo "3. Kembali ke Menu Utama"

		#get input from the user
		read -p "[Enter your choice [1-3] " pilihanB

		#make decision using case..in..esac
		case $pilihanB in
		1)
			echo "Nama : AGUS TRI SANTOSO"
			echo "NIM : 2197200882"
			echo "Home Dir : /home/agus"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		2)
			echo "agus@$(hostname)"
			read -p "Press [Enter] key to continue..."
			readEnterKey
			;;
		3)
			bash whiledo2
			;;
			*)
		echo "Error: Invalid option..."
		read -p "Press [Enter] key to continue..."
		readEnterKey
		;;
	esac
	;;

	C)
		clear
		exit 0
		;;
		*)
	echo "Error: Invalid option..."
	read -p "Press [Enter] key to continue..."
	readEnterKey
	;;
esac
done
;;